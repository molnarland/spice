const mix = require('laravel-mix');
const path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.disableSuccessNotifications();

mix
    .options({
        processCssUrls: false,
        autoprefixer: {
            options: {
                browsers: [
                    "Android 2.3",
                    "Android >= 4",
                    "Chrome >= 20",
                    "Firefox >= 24",
                    "Explorer >= 8",
                    "iOS >= 6",
                    "Opera >= 12",
                    "Safari >= 6"
                ]
            }
        }
    })
    .js('resources/js/main.ts', 'public/js')
    .sass('resources/sass/main.sass', 'public/css', {indentedSyntax: true})
    .webpackConfig({
        devtool: mix.inProduction() ? '' : 'inline-source-map',
        module: {
            rules: [{
                test: /\.tsx?$/,
                loader: 'ts-loader',
                options: {
                    appendTsSuffixTo: [/\.vue$/],
                },
                exclude: /node_modules/,
            }, {
                test: /\.pug$/,
                loader: 'pug-plain-loader'
            }/*, {
                test: /\.sass$/,
                use: [
                    'vue-style-loader', 
                    'css-loader', 
                    {
                        loader: 'sass-loader',
                        options: {
                            indentedSyntax: true
                        }
                    }
                ]
            }*/],
        },
        resolve: {
            extensions: ['*', '.js', '.jsx', '.vue', '.ts', '.tsx'],
            alias: {
                styles: path.resolve(__dirname, 'resources/assets/sass'),
                '@': path.resolve(__dirname, 'resources/assets/vue'),
            },
        },
    });

// Thanks https://github.com/JeffreyWay/laravel-mix/issues/1483#issuecomment-366685986
Mix.listen('configReady', (webpackConfig) => {
  if (Mix.isUsing('hmr')) {
    webpackConfig.entry = Object.keys(webpackConfig.entry).reduce((entries, entry) => {
      entries[entry.replace(/^\//, '')] = webpackConfig.entry[entry];
      return entries;
    }, {});

    webpackConfig.plugins.forEach((plugin) => {
      if (plugin.constructor.name === 'ExtractTextPlugin') {
        plugin.filename = plugin.filename.replace(/^\//, '');
      }
    });
  }
});
