<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        {{ Html::style('css/main.css') }}
    </head>
    <body>
        <div id="page-wrapper">
            <div id="welcome-wrapper">
                {{ Html::image('pics/background.jpg') }}
                <div id="welcome-text" class="text-right">
                    <h1>Welcome Back.</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at volutpat nunc.</p>
                </div>
            </div>
            <div id="auth-wrapper">
                <Auth/> 
            </div>
        </div>
        {{ Html::script('js/main.js') }}
    </body>
</html>
