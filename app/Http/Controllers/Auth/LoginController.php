<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Validator;

class LoginController extends Controller
{
    use AuthenticatesUsers;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     */
    public function login(Request $request)
    {
        $validate = $this->isValid($request);

        if ($validate !== true)
        {
            return response()
                ->json(['success' => false, 'msg' => $validate]);
        }

        $response = $this->attemptLogin($request) ? [
            'success' => true, 
            'msg' => ''
        ] : [
            'success' => false, 
            'msg' => [trans('auth.failed')]
        ];

        return response()
            ->json($response);
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return true;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return string
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return trans('auth.failed');
    }

    private function isValid(Request $request)
    {
        $validator = Validator::make($request->all(), [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $validator->errors()->all();
        }

        return true;
    }
}
